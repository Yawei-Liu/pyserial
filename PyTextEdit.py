#-*- coding: UTF-8 -*-
import sys
from PyQt4 import QtGui
from PyQt4.QtCore import QObject, SIGNAL
#from PySerial import PySerial

class PyTextEdit(QtGui.QTextEdit):
    def __init__(self, parent = None):
        super(PyTextEdit, self).__init__(parent)
        self.qObj = QObject()
        
    def keyPressEvent(self, e):
        self.tc = self.textCursor()
        self.tc.movePosition(self.tc.End)   
        self.setTextCursor(self.tc) 
        self.qObj.emit(SIGNAL('SerialSendData'), e.text())
        #self.insertPlainText(e.text())        

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    t = PyTextEdit("PyQt:Hello world!")    
    t.show()
    sys.exit(app.exec_())
